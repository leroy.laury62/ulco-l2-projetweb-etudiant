<?php

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();
session_start();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

// GET "/store"
$router->get('/store', 'controller\StoreController@store');

// GET "/store/{:num}"
$router->get('/store/{:num}', 'controller\StoreController@product');

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

// GET "/account"
$router->get('/account', 'controller\AccountController@account');

// POST "/account/login"
$router->post('/account/login', 'controller\AccountController@login');

// POST "/account/signin"
$router->post('/account/signin', 'controller\AccountController@signin');

// GET "/account/logout"
$router->get('/account/logout', 'controller\AccountController@logout');

// POST "/postComment"
$router->post('/postComment/{:num}', 'controller\CommentController@postComment');

// POST "/search"
$router->post('/search', 'controller\StoreController@search');

// GET "/account/infos"
$router->get('/account/infos', 'controller\AccountController@infos');

// POST "/account/update"
$router->post('/account/update', 'controller\AccountController@update');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
