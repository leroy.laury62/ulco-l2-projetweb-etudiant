<?php


namespace controller;


use model\CommentModel;

class CommentController
{
    public function postComment(int $id): void
    {
        $id_account = $_SESSION["userid"];
        $content = htmlspecialchars($_POST["content"]);
        $id_product = $id;
        CommentModel::insertComment($content, $id_product, $id_account);
        //On renvois l'utilisateur sur la même page pour qu'il voit son nouveau commentaire
        header("Location: /store/$id_product");
        exit();
    }
}