<?php


namespace controller;


use model\AccountModel;

class AccountController
{
    public function account(): void
    {

        if (isset($_SESSION["userid"])) {
            header("Location: /account/infos");
            exit;
        }
        $params = array(
            "title" => "compte",
            "module" => "account.php",
        );
        \view\Template::render($params);
    }

    public function signin(): void
    {
        $firstname = htmlspecialchars($_POST["userfirstname"]);
        $lastname = htmlspecialchars($_POST["userlastname"]);
        $mail = htmlspecialchars($_POST["usermail"]);
        //Chiffrement du mot de passe.
        $password = password_hash(htmlspecialchars($_POST["userpass"]), PASSWORD_DEFAULT);
        if (AccountModel::signin($firstname, $lastname, $mail, $password)) {
            header("Location: /account?status=signin_success");
            exit();
        } else {
            header("Location: /account?status=signin_fail");
            exit();
        }
    }

    public function infos(): void
    {
          if (!isset($_SESSION["userid"])) {
            header("Location: /account");
            exit;
        }
        $params = array(
            "title" => "compte",
            "module" => "infos.php",
        );
        \view\Template::render($params);
    }

    public function update(): void
    {
        $firstname = htmlspecialchars($_POST["firstname"]);
        $lastname = htmlspecialchars($_POST["lastname"]);
        $mail = htmlspecialchars($_POST["mail"]);
        if (AccountModel::update($firstname, $lastname, $mail)) {
            $params = array(
                "title" => "compte",
                "module" => "infos.php",
                "update" => true,
            );
            $_SESSION["userfirstname"] = $firstname;
            $_SESSION["userlastname"] = $lastname;
            $_SESSION["usermail"] = $mail;
            \view\Template::render($params);
        } else {
            $params = array(
                "title" => "compte",
                "module" => "infos.php",
                "update" => false,
            );
            \view\Template::render($params);
        }
    }

    public function login(): void
    {
        $mail = htmlspecialchars($_POST["usermail"]);
        //Chiffrement du mot de passe.
        $password = htmlspecialchars($_POST["userpass"]);
        $reponse = AccountModel::login($mail, $password);
        if ($reponse != 0) {
            $_SESSION["userfirstname"] = $reponse["firstname"];
            $_SESSION["userlastname"] = $reponse["lastname"];
            $_SESSION["usermail"] = $reponse["mail"];
            $_SESSION["userid"] = $reponse["id"];
            header("Location: /store");
            exit();
        } else {
            header("Location: /account?status=login_fail");
            exit();
        }
    }

    public function logout(): void
    {
        session_destroy();
        header("Location: /account?status=logout");
        exit();
    }
}