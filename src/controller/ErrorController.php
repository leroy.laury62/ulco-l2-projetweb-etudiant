<?php

namespace controller;

class ErrorController {

  public function error(): void
  {
      $params = [
          "title"  => "Error",
          "module" => "error.php"
      ];

      \view\Template::render($params);
  }

}