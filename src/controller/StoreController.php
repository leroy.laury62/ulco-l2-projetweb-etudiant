<?php

namespace controller;

class StoreController
{

    public function store(): void
    {
        // Communications avec la base de données
        $categories = \model\StoreModel::listCategories();
        $produit = \model\StoreModel::listProducts();

        // Variables à transmettre à la vue
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "product" => $produit
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);


    }

    public function search(): void
    {
        if (isset($_POST["search"])) {
            $search["search"] = htmlspecialchars($_POST["search"]);
        }
        if (isset($_POST["order"])) {
            $search["order"] = $_POST["order"];
        }
        if (isset($_POST["category"])) {
            $search["category"] = $_POST["category"];
        }
        // Communications avec la base de données
        $produit = \model\StoreModel::searchProduct($search);
        $categories = \model\StoreModel::listCategories();


        // Variables à transmettre à la vue
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "product" => $produit
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public function product(int $id): void
    {
        $produit = \model\StoreModel::infoProduct($id);
        $comment = \model\CommentModel::listComment($id);
        if ($produit == null) {
            header("Location: /store");
            exit();
        }
        $params = array(
            "title" => $produit["name"],
            "module" => "product.php",
            "product" => $produit,
            "comment" => $comment
        );
        \view\Template::render($params);
    }

}