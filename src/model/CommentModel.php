<?php


namespace model;


class CommentModel
{
    static function insertComment($content, $id_product, $id_account): bool
    {
        $db = \model\Model::connect();
        $sql = "INSERT INTO comment (id, content, id_product, id_account) VALUES(id, ?, ?, ?)";
        $req = $db->prepare($sql);
        $req->execute(array($content, $id_product, $id_account));
        return true;
    }

    static function listComment($id_product): array
    {
        $db = \model\Model::connect();
        $sql = "SELECT comment.*, account.firstname AS 'firstname', account.lastname AS 'lastname' FROM comment INNER JOIN account ON id_account = account.id WHERE id_product = '$id_product'";
        $req=$db->prepare($sql);
        $req->execute();
        $reponse = $req->fetchAll();
        return $reponse;
    }
}