<?php


namespace model;


class AccountModel
{
    static function check($firstname, $lastname, $mail, $password): bool
    {
        if (strlen($firstname) < 2 || strlen($lastname) < 2 || strlen($password) < 6 || !filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT $mail from account";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();
        if ($req->fetch() != 0) {
            return false;
        }
        return true;
    }

    static function signin($firstname, $lastname, $mail, $password): bool
    {
        if (!AccountModel::check($firstname, $lastname, $mail, $password)) return false;
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "INSERT INTO account VALUES (id, '$firstname', '$lastname', '$mail', '$password')";

        $req = $db->prepare($sql);
        $req->execute();
        return true;
    }

    static function login($mail, $password): ?array
    {
        $db = \model\Model::connect();
        $sql = "SELECT * FROM account WHERE mail = '$mail'";
        $req=$db->prepare($sql);
        $req->execute();
        $reponse = $req->fetch();
        if ($reponse==0 || !password_verify($password, $reponse["password"])) {
            echo $reponse["password"]. "\t" . $password;
            return null;
        }
        return $reponse;
    }

    static function update($firstname, $lastname, $mail): bool
    {
        $db = \model\Model::connect();
        //Vérification de la non existance de l'adresse mail
        $sql = "SELECT * FROM account WHERE mail = '$mail'";
        $req=$db->prepare($sql);
        $req->execute();
        $reponse = $req->fetch();
        if ($reponse!=null) {
            if ($reponse["mail"]!=$_SESSION["usermail"]) return false;
        }
        $sql = "UPDATE account SET firstname = '" . $firstname . "', lastname = '" . $lastname . "', mail = '" . $mail ."' WHERE account.id = " . $_SESSION["userid"];
        $req=$db->prepare($sql);
        $req->execute();
        return true;
    }

}