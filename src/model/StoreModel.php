<?php

namespace model;

class StoreModel
{

    static function listCategories(): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT id, name FROM category";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

    static function listProducts(): array
    {
        //Connexion à la bdd
        $db = \model\Model::connect();

        //requête SQl
        $sql = "SELECT product.id, product.name, price, image, category.name AS 'category.name' FROM product
                  INNER JOIN category WHERE product.category = category.id";

        //Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        return $req->fetchAll();
    }

    static function infoProduct(int $id): ?array
    {
        //Connexion à la bdd
        $db = \model\Model::connect();

        //requête SQl
        $sql = "SELECT product.id, product.name, price, image, image_alt1, image_alt2, image_alt3, spec, category.name AS 'category.name' FROM product
                INNER JOIN category WHERE product.category = category.id AND product.id = " . $id;

        //Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();
        $resultat = $req->fetch();
        return $resultat ? $resultat : null;
    }

    static function searchProduct(array $param): ?array
    {
        //Connexion à la bdd
        $db = \model\Model::connect();
        $search = null;
        $category = null;
        $order = null;
        if ($param["search"] != null) {
            $search = " && product.name LIKE '%" . $param['search'] . "%' ";
        }
        if (isset($param["order"])) {
            if ($param["order"] == "croissant") {
                $order = " ORDER BY product.price ASC";
            } else if ($param["order"] == "decroissant") {
                $order = " ORDER BY product.price DESC";
            }
        }
        if (isset($param["category"])) {
            $listeCategory = $param["category"];
            //On met une parenthèse pour la priorité des opérations
            $category .= " && (";
            foreach ($listeCategory as $categorie) {
                $category .= " category.name LIKE '" . $categorie . "'  || ";
            }
            //On retire le dernier || de category.
            $category = substr($category, 0, -4);
            //On oublie pas de fermer la parenthèse pour la priorité.
            $category .= ")";
        }

        //Requête initiale
        $sql = "SELECT product.id, product.name, price, image, category.name AS 'category.name' FROM product
                  INNER JOIN category WHERE product.category = category.id";

        //Concaténation des conditions à la requête initiale.
        $sql .= $search . $category . $order;
        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();
        //Retourner les résultats (type array)
        $resultat = $req->fetchAll();
        return $resultat ? $resultat : null;
    }
}