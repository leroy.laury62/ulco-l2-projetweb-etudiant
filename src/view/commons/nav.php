<nav>
    <img src="/public/images/logo.jpeg">
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>
    <a class="account" href="/account">
        <img src="/public/images/avatar.png">
    <?php
    if (isset($_SESSION['userfirstname'])) {
        echo $_SESSION['userfirstname'] . " " . $_SESSION['userlastname'];
        ?>
        <a href="/cart">Panier</a>
        <a href="/account/logout">Déconnexion</a>
    <?php } else { ?>
        Compte
    <?php } ?>
    </a>
</nav>