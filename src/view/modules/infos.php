<?php
if (isset($params["update"])) {
    if ($params["update"]==true) {
        echo "<div id='account' class='box info'>Vos informations ont bien été mises à jours !</div>";
    } else if ($params["update"]==false) {
        echo "<div id='account' class='box error'>Adresse mail déjà existante !!!</div>";
    }
}
?>
<div id="account">
    <form method="post" class="account-login" action="/account/update">
        <h1>Informations du compte</h1>
        <div>Informations personnelles</div>
        <p>Nom</p>
        <input id="firstname" type="text" name="firstname" value=<?= $_SESSION["userfirstname"] ?>>

        <p>Prénom</p>
        <input id="lastname" type="text" name="lastname" value=<?= $_SESSION["userlastname"] ?>>

        <p>Adresse Mail</p>
        <input id="mail" type="text" name="mail" value=<?= $_SESSION["usermail"] ?>>

        <input id="button" type="submit" value="Modifier mes informations"/>

    </form>
</div>
<div id="product" class="product-spec">
    <div>
        <div class="product-spec">
            <h2>Commandes</h2>
            <p>Aucune commande en cours</p>
        </div>
    </div>
</div>

<script src="/public/scripts/update.js"></script>