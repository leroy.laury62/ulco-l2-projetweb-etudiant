<?php

if (isset($_GET["status"])) {
    $status = $_GET["status"];
    $type_message = "info";
    if ($status == "login_fail") {
        $reponse = "La connexion a échoué. Vérifiez vos identifiants et réessayez.";
        $type_message = "error";
    } else if ($status == "signin_fail") {
        $reponse = "L'inscription a échoué. Vérifiez vos informations et réessayez.";
        $type_message = "error";
    } else if ($status == "signin_success") {
        $reponse = "Inscription réussie ! Vous pouvez dés à présent vous connecter";
    } else if ($status == "logout") {
        $reponse = "Vous êtes déconnecté. A bientôt !";
    }

    if ($type_message == "info") { ?>
        <div id="account" class="box info"><?= $reponse ?></div>
    <?php } else if ($type_message == "error") { ?>
        <div id="account" class="box error"><?= $reponse ?></div>
        <?php
    }
}
?>
<div id="account">

    <form class="account-login" method="post" action="/account/login">

        <h2>Connexion</h2>
        <h3>Tu as déjà un compte ?</h3>

        <p>Adresse mail</p>
        <input type="text" name="usermail" placeholder="Adresse mail"/>

        <p>Mot de passe</p>
        <input type="password" name="userpass" placeholder="Mot de passe"/>

        <input type="submit" value="Connexion"/>

    </form>

    <form class="account-signin" method="post" action="/account/signin">

        <h2>Inscription</h2>
        <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

        <p>Nom</p>
        <input type="text" name="userlastname" placeholder="Nom"/>

        <p>Prénom</p>
        <input type="text" name="userfirstname" placeholder="Prénom"/>

        <p>Adresse mail</p>
        <input type="text" name="usermail" placeholder="Adresse mail"/>

        <p>Mot de passe</p>
        <input type="password" name="userpass" placeholder="Mot de passe"/>

        <p>Répéter le mot de passe</p>
        <input type="password" name="userpass" placeholder="Mot de passe"/>

        <input type="submit" value="Inscription"/>

    </form>

</div>

<script src="/public/scripts/signin.js"></script>