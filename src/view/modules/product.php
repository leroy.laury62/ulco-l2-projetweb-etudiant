<?php
$product = $params["product"];
?>
<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?= $product['image'] ?>">
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?= $product['image'] ?>">
                </div>
                <div>
                    <img src="/public/images/<?= $product['image_alt1'] ?>">
                </div>
                <div>
                    <img src="/public/images/<?= $product['image_alt2'] ?>">
                </div>
                <div>
                    <img src="/public/images/<?= $product['image_alt3'] ?>">
                </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category"><?= $product["category.name"] ?></p>
            <h1><?= $product["name"] ?></h1>
            <p class="product-price"><?= $product["price"] ?>€</p>
            <form>
                <button type="button">-</button>
                <button type="button">1</button>
                <button type="button">+</button>
                <input type="submit">
            </form>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécifités</h2>
            <?= $product['spec'] ?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php
            $comments = $params["comment"];
            if ($comments==null){
            ?>
            <p>Il n'y a pas d'avis pour ce produit.</p>
            <?php } else {
                foreach ($comments as $commentaire){
                    echo "<div class='product-comment'>";
                    echo "<p class='product-comment-author'>" . $commentaire['firstname']. " " .$commentaire['lastname'] . "</p>";
                    echo "<p>" . $commentaire['content'] . "</p>";
                    echo "</div>";
                }
            }
            if (isset($_SESSION["userid"])){?>
            <form class="product-comment" method="post" action="/postComment/<?=$product["id"]?>">
                <input type="text" name="content" placeholder="Rédiger un commentaire"/>
            </form>
            <?php
            }
            ?>
        </div>
    </div>
</div>
<script src="/public/scripts/product.js"></script>