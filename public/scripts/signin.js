let userlastname = document.getElementsByName("userlastname")[0];
let userfirstname = document.getElementsByName("userfirstname")[0];
let usermail = document.getElementsByName("usermail")[1];
let password = document.getElementsByName("userpass")[1];
let repeatedpassword = document.getElementsByName("userpass")[2];
let bouton = document.getElementsByTagName("input")[8]
console.log(bouton);

document.addEventListener('DOMContentLoaded', function () {
    userfirstname.addEventListener("keyup", function () {
        if (userfirstname.value.length < 2) {
            userfirstname.classList.add("invalid")
            userfirstname.previousElementSibling.classList.add("invalid")
        } else {
            userfirstname.previousElementSibling.classList.remove("invalid")
            userfirstname.classList.remove("invalid")
            userfirstname.classList.add("valid")
            userfirstname.previousElementSibling.classList.add("valid")
        }
    })


    userlastname.addEventListener("keyup", function () {
        if (userlastname.value.length < 2) {
            userlastname.classList.add("invalid")
            userlastname.previousElementSibling.classList.add("invalid")
        } else {
            userlastname.previousElementSibling.classList.remove("invalid")
            userlastname.classList.remove("invalid")
            userlastname.classList.add("valid")
            userlastname.previousElementSibling.classList.add("valid")
        }
    })

    password.addEventListener("keyup", function () {
        if (password.value.length < 6 || !password.value.match(/^(?=.*\d)(?=.*[a-zA-Z])/)) {
            password.classList.add("invalid")
            password.previousElementSibling.classList.add("invalid")
        } else {
            password.previousElementSibling.classList.remove("invalid")
            password.classList.remove("invalid")
            password.classList.add("valid")
            password.previousElementSibling.classList.add("valid")
        }
    })

    repeatedpassword.addEventListener("keyup", function () {
        if (password.value !== repeatedpassword.value) {
            repeatedpassword.classList.add("invalid")
            repeatedpassword.previousElementSibling.classList.add("invalid")
        } else {
            repeatedpassword.previousElementSibling.classList.remove("invalid")
            repeatedpassword.classList.remove("invalid")
            repeatedpassword.classList.add("valid")
            repeatedpassword.previousElementSibling.classList.add("valid")
        }
    })

    usermail.addEventListener("keyup", function () {
        if (!usermail.value.match(/^(?=.*@.*\.)/)) {
            usermail.classList.add("invalid")
            usermail.previousElementSibling.classList.add("invalid")
        } else {
            usermail.previousElementSibling.classList.remove("invalid")
            usermail.classList.remove("invalid")
            usermail.classList.add("valid")
            usermail.previousElementSibling.classList.add("valid")
        }
    })

    bouton.addEventListener("click", function (evt) {
        if (userfirstname.value.length === 0 || userfirstname.className === "invalid" ||
            userlastname.value.length === 0 || userlastname.className === "invalid" ||
            usermail.value.length === 0 || usermail.className === "invalid" ||
            password.value.length === 0 || password.className === "invalid" ||
            repeatedpassword.value.length === 0 || repeatedpassword.className === "invalid") {
            evt.preventDefault();
        }
    })

});