//Div contenants les miniatures
let containeur = document.getElementsByClassName("product-miniatures");
//On stocke les miniatures pour appliquer l'event listener
let images = containeur[0].getElementsByTagName("img");

//On cible le div
let cible = document.getElementsByClassName("product-images")[0];

//Containeur permettant le ciblage des boutons, en cas de quantité = 5 on lui ajoutera le div d'erreur.
let containeurButtons = document.getElementsByClassName("product-infos")[0]
//On sélectionne les boutons
let buttons = containeurButtons.getElementsByTagName("button");

document.addEventListener('DOMContentLoaded', function () {
    //pour chaque image, on applique un listener sur le click qui change la source de l'image principale par la source de l'image cliquée
    for (let elem of images) {
        elem.addEventListener("click", function () {
            //on remplace la source de l'image par la source de notre élément.
            cible.firstElementChild.src = elem.src;
        })
    }

    //Listener sur le bouton moins
    buttons[0].addEventListener("click", function () {
        if (buttons[1].innerText > 1) {
            buttons[1].innerText--;
        }

        //On supprime le div d'erreur si il y en a un
        if (document.getElementsByClassName("box error").length !== 0) {
            let divErreur = document.getElementsByClassName("box error")[0];
            let parent = divErreur.parentNode;
            parent.removeChild(divErreur);
        }
    })

    //Listener sur le bouton plus
    buttons[2].addEventListener("click", function () {
        if (buttons[1].innerText < 5) {
            buttons[1].innerText++;
        }

        //Ajout du div d'erreur
        if (buttons[1].innerText === "5") {
            let div = document.createElement("div");
            div.className = "box error";
            div.innerText = "Quantité maximale autorisée !"
            //On vérifie si le dernier élément n'est pas déjà une div d'erreur.
            if (containeurButtons.lastChild.innerText !== div.innerText) {
                containeurButtons.append(div);
            }
        }
    })
})

