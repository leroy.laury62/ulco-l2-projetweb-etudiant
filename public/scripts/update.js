let userlastname = document.getElementById("lastname");
let userfirstname = document.getElementById("firstname");
let usermail = document.getElementById("mail");
let bouton = document.getElementById("button");

document.addEventListener('DOMContentLoaded', function () {
    userfirstname.addEventListener("keyup", function () {
        if (userfirstname.value.length < 2) {
            userfirstname.classList.add("invalid")
            userfirstname.previousElementSibling.classList.add("invalid")
        } else {
            userfirstname.previousElementSibling.classList.remove("invalid")
            userfirstname.classList.remove("invalid")
            userfirstname.classList.add("valid")
            userfirstname.previousElementSibling.classList.add("valid")
        }
    })

    userlastname.addEventListener("keyup", function () {
        if (userlastname.value.length < 2) {
            userlastname.classList.add("invalid")
            userlastname.previousElementSibling.classList.add("invalid")
        } else {
            userlastname.previousElementSibling.classList.remove("invalid")
            userlastname.classList.remove("invalid")
            userlastname.classList.add("valid")
            userlastname.previousElementSibling.classList.add("valid")
        }
    })

    usermail.addEventListener("keyup", function () {
        if (!usermail.value.match(/^(?=.*@.*\.)/)) {
            usermail.classList.add("invalid")
            usermail.previousElementSibling.classList.add("invalid")
        } else {
            usermail.previousElementSibling.classList.remove("invalid")
            usermail.classList.remove("invalid")
            usermail.classList.add("valid")
            usermail.previousElementSibling.classList.add("valid")
        }
    })

    bouton.addEventListener("click", function (evt) {
        if (userfirstname.value.length === 0 || userfirstname.className === "invalid" ||
            userlastname.value.length === 0 || userlastname.className === "invalid" ||
            usermail.value.length === 0 || usermail.className === "invalid"){
            evt.preventDefault();
        }
    })
});